-- Create gts_teacher table
create table gts_teacher(
    id int not null primary key auto_increment,
    code varchar(6) not null unique,
    name varchar(100) not null,
    date_of_birth date not null,
    created_at timestamp not null default current_timestamp,
    updated_at timestamp null on update current_timestamp
)   engine=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE utf8mb4_general_ci;