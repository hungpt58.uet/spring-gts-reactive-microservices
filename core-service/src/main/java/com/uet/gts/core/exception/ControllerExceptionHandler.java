package com.uet.gts.core.exception;

import com.uet.gts.common.dto.Response;
import com.uet.gts.common.exceptions.BusinessException;
import com.uet.gts.common.exceptions.RecordNotFoundException;
import com.uet.gts.common.utils.ErrorFactory;
import com.uet.gts.common.utils.LogUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ServerWebInputException;

import javax.naming.ServiceUnavailableException;
import javax.validation.ValidationException;
import java.util.stream.Collectors;

import static com.uet.gts.common.constant.ErrorList.*;

@Slf4j
@ControllerAdvice
public class ControllerExceptionHandler {

    @Autowired private ErrorFactory errorFactory;

    //Remember: Handle case: missing body from request
    @ExceptionHandler({ ValidationException.class, ServerWebInputException.class })
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody Response<?> handleServiceUnavailable(Exception e) {
        e.printStackTrace();
        return new Response<>(errorFactory.getI18NError(REQUEST_WRONG_FORMAT));
    }

    //Remember: Handle request body is failed by validation
    @ExceptionHandler(WebExchangeBindException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody Response<?> handleException(WebExchangeBindException e) {
        var errors = e.getAllErrors().stream().map(v -> (FieldError)v)
                .map(t -> errorFactory.getError(t.getDefaultMessage(), t.getField()))
                .collect(Collectors.toSet());
        return new Response<>(errors);
    }

    @ExceptionHandler(ServiceUnavailableException.class)
    @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
    public @ResponseBody Response<?> handleServiceUnavailable(ServiceUnavailableException e) {
        return new Response<>(errorFactory.getI18NError(SERVER_UNAVAILABLE));
    }

    @ExceptionHandler(RecordNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public @ResponseBody Response<?> handleRecordNotFound(RecordNotFoundException e) {
        return new Response<>(e.getError());
    }

    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody Response<?> handleBadBusiness(BusinessException e) {
        return new Response<>(e.getError());
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody Response<?> handleServerError(Throwable e) {
        log.error(LogUtil.convertStackTraceToString(e));
        return new Response<>(errorFactory.getI18NError(SERVER_ERROR));
    }
}
