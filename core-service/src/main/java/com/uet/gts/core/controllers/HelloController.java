package com.uet.gts.core.controllers;

import com.uet.gts.common.constant.apis.CorePath;
import com.uet.gts.core.component.HelloComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Optional;

@RestController
public class HelloController {

    @Value("${spring.application.name}")
    private String serviceName;

    @Autowired private HelloComponent helloComponent;

    @GetMapping(CorePath.HELLO)
    public Mono<String> sayHello(@RequestParam("name") Optional<String> name) {
        if (name.isEmpty()) return Mono.just(String.format("Hello, I'm %s ^^", serviceName));
        else                return Mono.just(String.format("Hello %s, I'm %s ^^", name.get(), serviceName));
    }

    @GetMapping(CorePath.TEST)
    public Mono<String> testCircuitBreaker() {
        return helloComponent.simulateCircuitBreaker();
    }
}
