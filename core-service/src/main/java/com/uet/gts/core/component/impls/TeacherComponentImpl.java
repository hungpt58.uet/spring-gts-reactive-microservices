package com.uet.gts.core.component.impls;

import com.uet.gts.common.dto.core.TeacherDTO;
import com.uet.gts.common.req.core.NewTeacherReq;
import com.uet.gts.common.req.core.StudentIdListReq;
import com.uet.gts.common.req.core.UpdateTeacherReq;
import com.uet.gts.core.component.TeacherComponent;
import com.uet.gts.core.exception.ExceptionFactory;
import com.uet.gts.core.model.mappers.TeacherMapper;
import com.uet.gts.core.service.StudentService;
import com.uet.gts.core.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashSet;
import java.util.Optional;

import static com.uet.gts.common.constant.ErrorList.ADD_STUDENT_FAILED;
import static com.uet.gts.common.constant.ErrorList.CODE_EXISTED;

@Service
public class TeacherComponentImpl implements TeacherComponent {

    @Autowired private StudentService studentService;
    @Autowired private TeacherService teacherService;
    @Autowired private ExceptionFactory exceptionFactory;

    public Mono<TeacherDTO> getById(Integer id) {
        return teacherService.findById(id, true)
                .mapNotNull(t -> TeacherMapper.convertToDTO(t, true))
                .switchIfEmpty(
                    exceptionFactory.recordNotFoundException(TeacherDTO.Fields.id)
                );
    }

    public Mono<Void> create(NewTeacherReq teacherReq) {
        return teacherService.findByCode(teacherReq.getCode())
                .flatMap(teacher ->
                        exceptionFactory.businessException(CODE_EXISTED, TeacherDTO.Fields.code)
                ).switchIfEmpty(
                    teacherService.createOrUpdate(
                            TeacherMapper.convertToEntity(teacherReq)
                    )
                ).then();
    }

    public Mono<Void> update(Integer teacherId, UpdateTeacherReq teacherReq) {
        return teacherService.findById(teacherId, false)
                .mapNotNull(teacher -> TeacherMapper.convertToEntity(teacherReq, teacher))
                .flatMap(teacher -> teacherService.createOrUpdate(teacher))
                .switchIfEmpty(
                    exceptionFactory.recordNotFoundException(TeacherDTO.Fields.id)
                )
                .then();
    }

    @Override
    public Mono<Void> addStudents(Integer teacherId, StudentIdListReq studentIdListReq) {
        var expectedStudentIds = new HashSet<>(studentIdListReq.getStudentIds());
        var m1 = teacherService.findById(teacherId, true)
            .switchIfEmpty(
                exceptionFactory.recordNotFoundException(TeacherDTO.Fields.id)
            );
        var m2 = studentService.findAndCheckByIds(expectedStudentIds);

        return Mono.zip(m1, m2).flatMap(tuple -> {
           var teacher = tuple.getT1();
           var inputStudents = tuple.getT2();

           var cannotAdd = !teacher.canAdd(inputStudents);
           if (cannotAdd) return exceptionFactory.businessException(ADD_STUDENT_FAILED, StudentIdListReq.Fields.studentIds);
           else {
               inputStudents.forEach(t -> t.setTeacherId(Optional.of(teacherId)));
               return studentService.createOrUpdate(inputStudents).collectList();
           }
        }).then();
    }

    @Override
    public Mono<Void> deleteStudents(Integer teacherId, StudentIdListReq studentIdListReq) {
        var expectedStudentIds = new HashSet<>(studentIdListReq.getStudentIds());
        var m1 = teacherService.findById(teacherId, true)
                .switchIfEmpty(
                    exceptionFactory.recordNotFoundException(TeacherDTO.Fields.id)
                );
        var m2 = studentService.findAndCheckByIds(expectedStudentIds);

        return Mono.zip(m1, m2).flatMap(tuple -> {
            var teacher = tuple.getT1();
            var inputStudents = tuple.getT2();

            var cannotRemove = !teacher.canRemove(inputStudents);
            if (cannotRemove) return exceptionFactory.businessException(ADD_STUDENT_FAILED, StudentIdListReq.Fields.studentIds);
            else {
                inputStudents.forEach(t -> t.setTeacherId(null));
                return studentService.createOrUpdate(inputStudents).collectList();
            }
        }).then();
    }

    @Override
    public Flux<TeacherDTO> getAll() {
        return teacherService.findAll(false)
                .map(t -> TeacherMapper.convertToDTO(t, false));
    }

}
