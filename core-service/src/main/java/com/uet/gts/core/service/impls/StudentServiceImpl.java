package com.uet.gts.core.service.impls;

import com.uet.gts.core.exception.ExceptionFactory;
import com.uet.gts.core.model.entities.StudentEntity;
import com.uet.gts.core.repositories.StudentRepository;
import com.uet.gts.core.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashSet;
import java.util.Set;

import static com.uet.gts.common.constant.ErrorList.STUDENT_IDS_INVALID;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired private StudentRepository studentRepository;
    @Autowired private ExceptionFactory exceptionFactory;

    @Override
    public Mono<StudentEntity> findById(Integer studentId) {
        return studentRepository.findById(studentId);
    }

    @Override
    public Mono<StudentEntity> createOrUpdate(StudentEntity student) {
        return studentRepository.save(student);
    }

    @Override
    public Mono<Void> deleteById(Integer studentId) {
        return studentRepository.deleteById(studentId);
    }

    @Override
    public Mono<StudentEntity> findByCode(String code) {
        return studentRepository.findByCode(code);
    }

    @Override
    public Mono<Set<StudentEntity>> findAndCheckByIds(Set<Integer> inputStudentIds) {
        return studentRepository.findAllById(inputStudentIds).collectList()
            .flatMap(students -> {
                var isValid = inputStudentIds.size() == students.size();
                if (isValid) return Mono.just(new HashSet<>(students));
                else         return exceptionFactory.businessException(STUDENT_IDS_INVALID, null);
            });
    }

    @Override
    public Flux<StudentEntity> createOrUpdate(Set<StudentEntity> students) {
        return studentRepository.saveAll(students);
    }

    @Override
    public Flux<StudentEntity> findAll() {
        return studentRepository.findAll();
    }
}
