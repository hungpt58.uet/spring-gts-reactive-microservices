package com.uet.gts.core.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.resilience4j.bulkhead.BulkheadConfig;
import io.github.resilience4j.bulkhead.ThreadPoolBulkhead;
import io.github.resilience4j.bulkhead.ThreadPoolBulkheadConfig;
import io.github.resilience4j.bulkhead.internal.SemaphoreBulkhead;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RateLimiterConfig;
import io.github.resilience4j.ratelimiter.RateLimiterRegistry;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;
import org.springframework.cloud.circuitbreaker.resilience4j.ReactiveResilience4JCircuitBreakerFactory;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.web.reactive.config.WebFluxConfigurer;

import java.time.Duration;

@Configuration
public class WebFluxConfig implements WebFluxConfigurer {

    @Override
    public void configureHttpMessageCodecs(ServerCodecConfigurer configurer) {
        configurer.defaultCodecs().maxInMemorySize(512 * 1024);
    }

    @Bean
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    //============================[ Circuitbreaker ]============================================
    private CircuitBreakerConfig configCircuitBreaker() {
        return CircuitBreakerConfig.custom()
            .failureRateThreshold(20)
            .slowCallRateThreshold(50)
            .waitDurationInOpenState(Duration.ofSeconds(30))
            .slowCallDurationThreshold(Duration.ofSeconds(2))
            .permittedNumberOfCallsInHalfOpenState(3)
            .minimumNumberOfCalls(5)
            .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
            .slidingWindowSize(10)
            .build();
    }

    private TimeLimiterConfig configTimeLimit() {
        return TimeLimiterConfig.custom()
            .cancelRunningFuture(true)
            .timeoutDuration(Duration.ofSeconds(60))
            .build();
    }

    @Bean
    public Customizer<ReactiveResilience4JCircuitBreakerFactory> slowCustomizer() {
        return factory -> {
            factory.configure(builder -> builder
                .timeLimiterConfig(configTimeLimit())
                .circuitBreakerConfig(configCircuitBreaker()).build(), "anotherService");

            factory.addCircuitBreakerCustomizer(circuitBreaker -> circuitBreaker.getEventPublisher()
                .onError((t) -> {
                    System.out.printf("ON_ERROR_CIRCUIT_BREAKER: %s - %s\n", t.getCircuitBreakerName(), t.getEventType().name());
                }).onSuccess(t -> {
                    System.out.printf("ON_SUCCESS_CIRCUIT_BREAKER: %s - %s\n", t.getCircuitBreakerName(), t.getEventType().name());
                }).onCallNotPermitted(t -> {
                    System.out.printf("ON_CallNotPermitted_CIRCUIT_BREAKER: %s - %s\n", t.getCircuitBreakerName(), t.getEventType().name());
                }), "anotherService");
        };
    }

    //=============================[ Bulkhead config ]===========================================
    /**
     * Apply Bulkhead by ThreadPoolBulkhead && SemaphoreBulkhead (Bulkhead)
     */

    @Bean
    public ThreadPoolBulkhead getThreadPoolBulkhead() {
        ThreadPoolBulkheadConfig config = ThreadPoolBulkheadConfig.custom()
            .maxThreadPoolSize(3)
            .coreThreadPoolSize(2)
            .queueCapacity(20)
            .build();
        return ThreadPoolBulkhead.of("anotherService", config);
    }

    @Bean
    public SemaphoreBulkhead getSemaphoreBulkhead() {
        BulkheadConfig config = BulkheadConfig.custom()
            .maxConcurrentCalls(2) // only 2 thread access resource at same time.
            .maxWaitDuration(Duration.ofSeconds(1)) // Another threads wait in 60s to access resource. If over, threads stop
            .build();

        return new SemaphoreBulkhead("anotherService", config);
    }

    //=========================[ RateLimiter ]======================================
    @Bean
    public RateLimiter getRateLimiterConfig() {
        var config = RateLimiterConfig.custom()
            .limitRefreshPeriod(Duration.ofSeconds(5)) // Every 5s, send ${limitForPeriod} request
            .limitForPeriod(1) // as config: 1 request / 5s
            .timeoutDuration(Duration.ofSeconds(60))
            .build();
        var rateLimiterRegistry = RateLimiterRegistry.of(config);

        return rateLimiterRegistry.rateLimiter("anotherService");
    }

    //=====================[ Retry ]=================================
    @Bean
    @Primary
    public Retry getRetry() {
        var config = RetryConfig.custom()
            .waitDuration(Duration.ofSeconds(5)) //A fixed wait duration between retry attempts
            .maxAttempts(5)
            .failAfterMaxAttempts(true)
            .build();
        RetryRegistry registry = RetryRegistry.of(config);

        return registry.retry("anotherService");
    }
}
