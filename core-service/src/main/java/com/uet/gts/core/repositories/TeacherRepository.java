package com.uet.gts.core.repositories;

import com.uet.gts.core.model.entities.TeacherEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface TeacherRepository extends ReactiveCrudRepository<TeacherEntity, Integer> {
    Mono<TeacherEntity> findByCode(String code);
}
