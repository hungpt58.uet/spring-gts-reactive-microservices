package com.uet.gts.core.model.mappers;

import com.uet.gts.common.dto.core.StudentDTO;
import com.uet.gts.common.enumeration.Gender;
import com.uet.gts.common.req.core.NewStudentReq;
import com.uet.gts.common.req.core.UpdateStudentReq;
import com.uet.gts.core.model.entities.StudentEntity;

public class StudentMapper {

    //======================[ Convert 2 DTO ]========================================
    public static StudentDTO convertToDTO(StudentEntity student) {
        if (student == null) return null;

        return StudentDTO.builder()
                .id(student.getId())
                .code(student.getCode())
                .name(student.getName())
                .age(student.getAge())
                .status(student.getStatus().toString())
                .build();
    }

    //======================[ Convert 2 Entity ]========================================
    public static StudentEntity convertToEntity(NewStudentReq studentReq) {
        if (studentReq == null) return null;

        return StudentEntity.builder()
                .code(studentReq.getCode())
                .dateOfBirth(studentReq.getDateOfBirth())
                .name(studentReq.getName())
                .gender(Gender.valueOf(studentReq.getGender().toUpperCase()))
                .build();
    }

    public static StudentEntity convertToEntity(UpdateStudentReq studentReq, StudentEntity student){
        if (studentReq == null || student == null) return null;

        student.setName(studentReq.getName());
        student.setDateOfBirth(studentReq.getDateOfBirth());

        return student;
    }
}
