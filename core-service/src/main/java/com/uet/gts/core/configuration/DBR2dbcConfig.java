package com.uet.gts.core.configuration;

import com.uet.gts.common.enumeration.converters.GenderReader;
import com.uet.gts.common.enumeration.converters.GenderWriter;
import com.uet.gts.common.enumeration.converters.OptionalFieldReader;
import com.uet.gts.common.enumeration.converters.OptionalFieldWriter;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;

import java.util.List;

@Configuration
public class DBR2dbcConfig extends AbstractR2dbcConfiguration {

    @Autowired private ConnectionFactory connectionFactory;

    @Override
    public ConnectionFactory connectionFactory() {
        return connectionFactory;
    }

    @Override
    protected List<Object> getCustomConverters() {
        return List.of(
            new GenderReader(),
            new GenderWriter(),
            new OptionalFieldReader(),
            new OptionalFieldWriter()
        );
    }
}
