package com.uet.gts.core.model.mappers;

import com.uet.gts.common.dto.core.StudentDTO;
import com.uet.gts.common.dto.core.TeacherDTO;
import com.uet.gts.common.req.core.NewTeacherReq;
import com.uet.gts.common.req.core.UpdateTeacherReq;
import com.uet.gts.core.model.entities.StudentEntity;
import com.uet.gts.core.model.entities.TeacherEntity;

import java.util.Set;
import java.util.stream.Collectors;

public class TeacherMapper {

    //======================[ Convert 2 DTO ]========================================
    public static TeacherDTO convertToDTO(TeacherEntity teacher, boolean includeStudents) {
        if (teacher == null) return null;

        Set<StudentDTO> studentDTOs = null;
        if (includeStudents) {
            studentDTOs = teacher.getStudents().stream()
                    .map(TeacherMapper::convertToDTO)
                    .collect(Collectors.toSet());
        }

        return TeacherDTO.builder()
                .id(teacher.getId())
                .name(teacher.getName())
                .code(teacher.getCode())
                .students(studentDTOs)
                .build();
    }

    private static StudentDTO convertToDTO(StudentEntity student) {
        return StudentDTO.builder()
                .id(student.getId())
                .code(student.getCode())
                .name(student.getName())
                .build();
    }

    //======================[ Convert 2 Entity ]========================================
    public static TeacherEntity convertToEntity(NewTeacherReq teacherReq) {
        if (teacherReq == null) return null;

        return new TeacherEntity(
                null,
                teacherReq.getCode(),
                teacherReq.getName(),
                teacherReq.getDateOfBirth()
        );
    }

    public static TeacherEntity convertToEntity(UpdateTeacherReq teacherReq, TeacherEntity teacher) {
        if (teacherReq == null || teacher == null) return null;

        teacher.setDateOfBirth(teacherReq.getDateOfBirth());
        teacher.setName(teacherReq.getName());

        return teacher;
    }
}
