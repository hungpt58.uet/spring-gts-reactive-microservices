package com.uet.gts.core.configuration;

import com.uet.gts.common.constant.UserRole;
import com.uet.gts.core.exception.AccessDeniedExceptionHandler;
import com.uet.gts.core.exception.AuthenticationFailedExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverterAdapter;
import org.springframework.security.oauth2.server.resource.web.server.ServerBearerTokenAuthenticationConverter;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.ServerAuthenticationConverter;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import reactor.core.publisher.Mono;

import static com.uet.gts.common.constant.apis.CorePath.*;

@Configuration
@EnableWebFluxSecurity
public class WebSecurityHttpConfig {

    @Autowired private AuthenticationFailedExceptionHandler authenticationFailedExceptionHandler;
    @Autowired private AccessDeniedExceptionHandler accessDeniedExceptionHandler;

    private static final String AUTHORITIES = "authorities";

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        return http.cors().and()
            .csrf().disable()
            .authorizeExchange()
            .pathMatchers(HELLO, TEST).permitAll()
            .pathMatchers(Internal.TEACHERS, Internal.TEACHER).permitAll()
            .pathMatchers(SWAGGER, V3_DOC, WEBJARS).hasAuthority(UserRole.OPERATOR)
            .pathMatchers(HttpMethod.DELETE, STUDENT).hasAnyAuthority(UserRole.DIRECTOR, UserRole.OPERATOR)
            .pathMatchers(STUDENTS, STUDENT).hasAnyAuthority(UserRole.DIRECTOR, UserRole.OPERATOR, UserRole.MANAGER)
            .pathMatchers(TEACHERS, TEACHER).hasAnyAuthority(UserRole.DIRECTOR, UserRole.OPERATOR, UserRole.GENERAL_MANAGER)
            .pathMatchers(TEACHER_STUDENT).hasAnyAuthority(UserRole.DIRECTOR, UserRole.OPERATOR)
            .anyExchange().authenticated()
            .and().oauth2ResourceServer()
            .bearerTokenConverter(serverAuthenticationConverter())
            .jwt()
            .jwtAuthenticationConverter(jwtAuthenticationConverter())
            .and().accessDeniedHandler(accessDeniedExceptionHandler)
            .authenticationEntryPoint(authenticationFailedExceptionHandler)
            .and().build();
    }

    @Bean
    public Converter<Jwt, ? extends Mono<? extends AbstractAuthenticationToken>> jwtAuthenticationConverter() {
        var grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        grantedAuthoritiesConverter.setAuthoritiesClaimName(AUTHORITIES);
        grantedAuthoritiesConverter.setAuthorityPrefix("");
        var jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
        return new ReactiveJwtAuthenticationConverterAdapter(jwtAuthenticationConverter);
    }

    //TODO: Check later when login
    private Mono<AuthorizationDecision> currentUserMatchesPath(Mono<Authentication> authentication, AuthorizationContext context) {
        return authentication
            .map(v -> context.getVariables().get("user").equals(v.getName()))
            .map(granted -> new AuthorizationDecision(granted));
    }

    //Remember: Can override `ServerBearerTokenAuthenticationConverter` for customize jwt converter
    @Bean
    public ServerAuthenticationConverter serverAuthenticationConverter() {
        var serverAuthenticationConverter = new ServerBearerTokenAuthenticationConverter();
        serverAuthenticationConverter.setAllowUriQueryParameter(true);
        return serverAuthenticationConverter;
    }
}
