package com.uet.gts.core.controllers;

import com.uet.gts.common.constant.apis.CorePath;
import com.uet.gts.common.dto.Response;
import com.uet.gts.common.req.core.NewStudentReq;
import com.uet.gts.common.req.core.UpdateStudentReq;
import com.uet.gts.core.component.StudentComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
public class StudentController {
    @Autowired private StudentComponent studentComponent;

    @GetMapping(path = CorePath.STUDENTS)
    public Mono<?> getAllStudents() {
        return studentComponent.getAll()
                .collectList()
                .map(t -> new Response<>(t));
    }

    @GetMapping(path = CorePath.STUDENT)
    public Mono<?> getStudentById(@PathVariable("id") Integer studentId) {
        return studentComponent.getById(studentId)
                .map(dto -> new Response<>(dto));
    }

    @PostMapping(path = CorePath.STUDENTS)
    public Mono<?> createNewStudent(@Valid @RequestBody NewStudentReq studentReq) {
        return studentComponent.create(studentReq)
            .then(Mono.just(new Response<>()));
    }

    @PutMapping(path = CorePath.STUDENT)
    public Mono<?> updateStudent(@PathVariable("id") Integer id, @Valid @RequestBody UpdateStudentReq studentReq) {
        return studentComponent.update(id, studentReq)
                .then(Mono.just(new Response<>()));
    }

    @DeleteMapping(path = CorePath.STUDENT)
    public Mono<?> deleteStudentById(@PathVariable("id") Integer id) {
        return studentComponent.deleteById(id)
                .then(Mono.just(new Response<>()));
    }
}
