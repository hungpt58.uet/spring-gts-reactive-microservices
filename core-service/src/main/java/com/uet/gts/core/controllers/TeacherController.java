package com.uet.gts.core.controllers;

import com.uet.gts.common.constant.apis.CorePath;
import com.uet.gts.common.dto.Response;
import com.uet.gts.common.req.core.NewTeacherReq;
import com.uet.gts.common.req.core.StudentIdListReq;
import com.uet.gts.common.req.core.UpdateTeacherReq;
import com.uet.gts.core.component.TeacherComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Validated
@RestController
public class TeacherController {
    @Autowired private TeacherComponent teacherComponent;

    @GetMapping(path = CorePath.TEACHERS)
    public Mono<?> getAllTeachers() {
        return teacherComponent.getAll()
                .collectList()
                .map(t -> new Response<>(t));
    }

    @GetMapping(path = CorePath.Internal.TEACHERS)
    public Mono<?> getInternalAllTeachers() {
        return getAllTeachers();
    }

    @GetMapping(path = CorePath.TEACHER)
    public Mono<?> getTeacherById(@PathVariable("id") Integer teacherId) {
        return teacherComponent.getById(teacherId)
                .map(dto -> new Response<>(dto));
    }

    @GetMapping(path = CorePath.Internal.TEACHER)
    public Mono<?> getInternalTeacherById(@PathVariable("id") Integer teacherId) {
        return getTeacherById(teacherId);
    }

    @PostMapping(path = CorePath.TEACHERS)
    public Mono<?> createNewTeacher(@Valid @RequestBody NewTeacherReq teacherReq) {
        return teacherComponent.create(teacherReq)
                .then(Mono.just(new Response<>()));
    }

    @PutMapping(path = CorePath.TEACHER)
    public Mono<?> updateTeacher(@PathVariable("id") Integer id, @Valid @RequestBody UpdateTeacherReq teacherReq) {
        return teacherComponent.update(id, teacherReq)
                .then(Mono.just(new Response<>()));
    }

    @PutMapping(path = CorePath.TEACHER_STUDENT)
    public Mono<?> addStudents(@PathVariable("id") Integer teacherId, @Valid @RequestBody StudentIdListReq studentIdListReq) {
        return teacherComponent.addStudents(teacherId, studentIdListReq)
                .then(Mono.just(new Response<>()));
    }

    @DeleteMapping(path = CorePath.TEACHER_STUDENT)
    public Mono<?> deleteStudents(@PathVariable("id") Integer teacherId, @Valid @RequestBody StudentIdListReq studentIdListReq) {
        return teacherComponent.deleteStudents(teacherId, studentIdListReq)
                .then(Mono.just(new Response<>()));
    }

}
