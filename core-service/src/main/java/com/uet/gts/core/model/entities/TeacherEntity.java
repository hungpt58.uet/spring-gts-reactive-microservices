package com.uet.gts.core.model.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceCreator;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Table(name = "gts_teacher")
public class TeacherEntity implements Serializable {
    private static final long serialVersionUID = 6702166127338067041L;

    @Id
    @Column(value = "id")
    private Integer id;

    @Column(value = "code")
    private String code;

    @Column(value = "name")
    private String name;

    @Column(value = "date_of_birth")
    private LocalDate dateOfBirth;

    @Transient
    private Set<StudentEntity> students = new HashSet<>();

    @PersistenceCreator
    public TeacherEntity(Integer id, String code, String name, LocalDate dateOfBirth) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }

    //=================[ Business method ]=======================
    @Transient
    public static final Integer MAX_STUDENT = 15;

    public boolean canAdd(Set<StudentEntity> students) {
        var isFree = students.stream().allMatch(t -> t.getTeacherId().isEmpty());
        if (!isFree) return false;

        var currentIds = getCurrentStudentIds();
        var newIds = students.stream().map(v -> v.getId()).collect(Collectors.toSet());
        var isValidSize = (currentIds.size() + newIds.size()) < MAX_STUDENT;
        var isNoDuplicatedStudent = newIds.stream().noneMatch(id -> currentIds.contains(id));

        return isValidSize && isNoDuplicatedStudent;
    }

    public boolean canRemove(Set<StudentEntity> students) {
        return students.stream()
                .allMatch(t -> t.getTeacherId().stream().allMatch(v -> v == this.id));
    }

    private Set<Integer> getCurrentStudentIds() {
        return students.stream().map(v -> v.getId()).collect(Collectors.toSet());
    }
}
