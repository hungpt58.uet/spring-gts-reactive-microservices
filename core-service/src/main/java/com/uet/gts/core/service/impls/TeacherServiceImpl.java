package com.uet.gts.core.service.impls;

import com.uet.gts.core.model.entities.TeacherEntity;
import com.uet.gts.core.repositories.StudentRepository;
import com.uet.gts.core.repositories.TeacherRepository;
import com.uet.gts.core.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.HashSet;

@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired private TeacherRepository teacherRepository;
    @Autowired private StudentRepository studentRepository;

    @Override
    @Cacheable(value = "teachers", condition = "#canIncludeStudent==false")
    public Flux<TeacherEntity> findAll(boolean canIncludeStudent) {
        var teacherFlux = teacherRepository.findAll();
        if (!canIncludeStudent) return teacherFlux;
        else {
            return teacherFlux
                .parallel(2)
                .runOn(Schedulers.boundedElastic())
                .flatMap(teacher -> {
                    return studentRepository.findByTeacherId(teacher.getId())
                        .collectList().map(students -> {
                            teacher.setStudents(new HashSet<>(students));
                            return teacher;
                        });
                }).sequential();
        }
    }

    @Override
    @Cacheable(value = "teacher")
    public Mono<TeacherEntity> findById(Integer id, boolean canIncludeStudent) {
        var teacherMono = teacherRepository.findById(id);
        if (!canIncludeStudent) return teacherMono;
        else {
            return teacherMono.flatMap(teacher -> {
                return studentRepository.findByTeacherId(teacher.getId())
                    .collectList().map(students -> {
                        teacher.setStudents(new HashSet<>(students));
                        return teacher;
                    });
            });
        }
    }

    @Override
    public Mono<TeacherEntity> findByCode(String code) {
        return teacherRepository.findByCode(code);
    }

    @Override
    @Caching(
        put = @CachePut(value = "teacher", unless = "#teacher.id!=null"), //Update cache
        evict = { @CacheEvict(value = "teachers")} //Remove cache teacher list
    )
    public Mono<TeacherEntity> createOrUpdate(TeacherEntity teacher) {
        return teacherRepository.save(teacher);
    }


}
