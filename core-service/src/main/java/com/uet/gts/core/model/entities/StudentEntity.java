package com.uet.gts.core.model.entities;

import com.uet.gts.common.enumeration.Gender;
import com.uet.gts.common.enumeration.StudentStatus;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Optional;

@Data
@Builder
@Table(value = "gts_student")
public class StudentEntity implements Serializable {
    private static final long serialVersionUID = 6702166127338067041L;

    @Id
    @Column(value = "id")
    private Integer id;

    @Column(value = "code")
    private String code;

    @Column(value = "name")
    private String name;

    @Column(value = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(value = "gender")
    private Gender gender;

    @Column(value = "teacher_id")
    private Optional<Integer> teacherId;
    //===================[ Business Method ]=============================
    @Transient
    public static final Integer START_AGE = 18;
    @Transient
    public static final Integer END_AGE   = 22;

    public int getAge() {
        var yearOfBirth = LocalDate.parse(dateOfBirth.toString()).getYear();
        var currentYear = LocalDate.now().getYear();

        return currentYear - yearOfBirth;
    }

    public int getGrade() {
        var currentAge = getAge();
        var isInStudyRange = START_AGE <= currentAge && currentAge <= END_AGE;

        if (isInStudyRange) return currentAge - START_AGE + 1;
        else                return 0;
    }

    public StudentStatus getStatus() {
        var currentAge = getAge();

        if (currentAge < START_AGE)    return StudentStatus.NOT_JOIN;
        else if (currentAge > END_AGE) return StudentStatus.JOINED;
        else                           return StudentStatus.GRADUATED;
    }

}
