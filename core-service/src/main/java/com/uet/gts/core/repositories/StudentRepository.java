package com.uet.gts.core.repositories;

import com.uet.gts.core.model.entities.StudentEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface StudentRepository extends ReactiveCrudRepository<StudentEntity, Integer> {
    Flux<StudentEntity> findByTeacherId(Integer teacherId);

    Mono<StudentEntity> findByCode(String code);
}
