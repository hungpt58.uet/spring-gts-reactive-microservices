package com.uet.gts.core.service;

import com.uet.gts.core.model.entities.TeacherEntity;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TeacherService {
    Flux<TeacherEntity> findAll(boolean canIncludeStudent);
    Mono<TeacherEntity> findById(Integer id, boolean canIncludeStudent);
    Mono<TeacherEntity> findByCode(String code);
    Mono<TeacherEntity> createOrUpdate(TeacherEntity teacher);
}
