package com.uet.gts.core.exception;

import com.uet.gts.common.exceptions.BusinessException;
import com.uet.gts.common.exceptions.RecordNotFoundException;
import com.uet.gts.common.utils.ErrorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static com.uet.gts.common.constant.ErrorList.RECORD_NOT_FOUND;

@Component
public class ExceptionFactory {

    @Autowired private ErrorFactory errorFactory;

     public <T> Mono<T> recordNotFoundException(String fieldName) {
        return Mono.error(
                new RecordNotFoundException(
                        errorFactory.getI18NError(RECORD_NOT_FOUND, fieldName)
                )
        );
    }

    public <T> Mono<T> businessException(String i18nKey, String fieldName) {
        return Mono.error(
                new BusinessException(
                        errorFactory.getI18NError(i18nKey, fieldName)
                )
        );
    }
}
