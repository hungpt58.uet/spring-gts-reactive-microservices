package com.uet.gts.core.component.impls;

import com.uet.gts.common.dto.core.StudentDTO;
import com.uet.gts.common.req.core.NewStudentReq;
import com.uet.gts.common.req.core.UpdateStudentReq;
import com.uet.gts.core.component.StudentComponent;
import com.uet.gts.core.exception.ExceptionFactory;
import com.uet.gts.core.model.mappers.StudentMapper;
import com.uet.gts.core.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.uet.gts.common.constant.ErrorList.CODE_EXISTED;

@Service
public class StudentComponentImpl implements StudentComponent {

    @Autowired private StudentService studentService;
    @Autowired private ExceptionFactory exceptionFactory;

    @Override
    public Mono<StudentDTO> getById(Integer studentId) {
        return studentService.findById(studentId)
                .mapNotNull(StudentMapper::convertToDTO)
                .switchIfEmpty(
                    exceptionFactory.recordNotFoundException(StudentDTO.Fields.id)
                );
    }

    @Override
    public Mono<Void> create(NewStudentReq studentReq) {
        return studentService.findByCode(studentReq.getCode())
                .flatMap(teacher ->
                    exceptionFactory.businessException(CODE_EXISTED, StudentDTO.Fields.code)
                ).switchIfEmpty(
                    studentService.createOrUpdate(
                        StudentMapper.convertToEntity(studentReq)
                    )
                ).then();
    }

    @Override
    public Mono<Void> update(Integer studentId, UpdateStudentReq studentReq) {
        return studentService.findById(studentId)
                .mapNotNull(student -> StudentMapper.convertToEntity(studentReq, student))
                .flatMap(student -> studentService.createOrUpdate(student))
                .switchIfEmpty(
                    exceptionFactory.recordNotFoundException(StudentDTO.Fields.id)
                ).then();
    }

    @Override
    public Mono<Void> deleteById(Integer studentId) {
        return studentService.findById(studentId)
                .switchIfEmpty(
                    exceptionFactory.recordNotFoundException(StudentDTO.Fields.id)
                )
                .flatMap(student -> studentService.deleteById(studentId))
                .then();
    }

    @Override
    public Flux<StudentDTO> getAll() {
        return studentService.findAll().map(StudentMapper::convertToDTO);
    }
}
