package com.uet.gts.core.component;

import com.uet.gts.common.dto.core.TeacherDTO;
import com.uet.gts.common.req.core.NewTeacherReq;
import com.uet.gts.common.req.core.StudentIdListReq;
import com.uet.gts.common.req.core.UpdateTeacherReq;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TeacherComponent {
    Mono<TeacherDTO> getById(Integer id);
    Mono<Void> create(NewTeacherReq teacherReq);
    Mono<Void> update(Integer teacherId, UpdateTeacherReq teacherReq);
    Mono<Void> addStudents(Integer teacherId, StudentIdListReq studentIdListReq);
    Mono<Void> deleteStudents(Integer teacherId, StudentIdListReq studentIdListReq);

    Flux<TeacherDTO> getAll();
}
