package com.uet.gts.core.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uet.gts.common.dto.Response;
import com.uet.gts.common.utils.ErrorFactory;
import lombok.SneakyThrows;
import org.apache.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.server.authorization.ServerAccessDeniedHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

import static com.uet.gts.common.constant.ErrorList.SERVER_FORBIDDEN;

@Component
public class AccessDeniedExceptionHandler implements ServerAccessDeniedHandler {

    @Autowired private ErrorFactory errorFactory;
    @Autowired private ObjectMapper objectMapper;

    @SneakyThrows
    @Override
    public Mono<Void> handle(ServerWebExchange exchange, AccessDeniedException denied) {
        ServerHttpResponse response= exchange.getResponse();
        response.setStatusCode(HttpStatus.FORBIDDEN);
        response.getHeaders().add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        
        var errorDTO = errorFactory.getI18NError(SERVER_FORBIDDEN);
        String body = objectMapper.writeValueAsString(new Response<>(errorDTO));
        DataBuffer buffer = response.bufferFactory().wrap(body.getBytes(StandardCharsets.UTF_8));
        return response.writeWith(Mono.just(buffer));
    }
}
