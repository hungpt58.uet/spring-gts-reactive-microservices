package com.uet.gts.core.component.impls;

import com.uet.gts.core.component.HelloComponent;
import io.github.resilience4j.bulkhead.Bulkhead;
import io.github.resilience4j.bulkhead.ThreadPoolBulkhead;
import io.github.resilience4j.bulkhead.internal.SemaphoreBulkhead;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.retry.Retry;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.circuitbreaker.ReactiveCircuitBreakerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class HelloComponentImpl implements HelloComponent {

    @Autowired private ReactiveCircuitBreakerFactory circuitBreakerFactory;;
    @Autowired private ThreadPoolBulkhead threadPoolBulkhead;
    @Autowired private SemaphoreBulkhead semaphoreBulkhead;
    @Autowired private RateLimiter rateLimiter;
    @Autowired private Retry retry;

    @Value("${resilience4j.test.enabled}")
    private Boolean enabledTestResilience;

    private AtomicInteger counter = new AtomicInteger(0);

    @Override
    public Mono<String> simulateCircuitBreaker() {
        return circuitBreakerFactory.create("anotherService").run(
            WebClient.builder().build()
                .get().uri("http://127.0.0.1:1337")
                .retrieve()
                .bodyToMono(String.class)
        );
    }

    @PostConstruct
    @SneakyThrows
    public void testBulkhead() {
        if (enabledTestResilience) {
            CompletableFuture.runAsync(() -> testRetry(), Executors.newFixedThreadPool(1));
            CompletableFuture.runAsync(() -> {
                for(int i = 0; i < 5; i++) {
                    testRateLimiter(i);
                    testThreadpoolBulkhead();
                    testSemaphoreBulkhead(i).thenAccept(v -> System.out.println("DONE: " + v.toString()));
                }
            });
        }
    }

    /**
     * There are 5 threads want to access this method. If `maxConcurrentCalls` = 2
     * > Thread_0, Thread_1 access and Thread_2, Thread_3, Thread_4 wait
     * > If over `maxWaitDuration`, Thread_2, Thread_3, Thread_4 will be cancelled
     * > If lower `maxWaitDuration`, next two threads: 2 & 3 and Thread_4 keep waiting
     */
    private CompletionStage<Integer> testSemaphoreBulkhead(int i) {
        return Bulkhead.decorateCompletionStage(semaphoreBulkhead, () -> {
            return CompletableFuture.supplyAsync(() -> {
                try {
                    System.out.println("Start testSemaphoreBulkhead: " + i + "_" + Thread.currentThread().getName());
                    Thread.sleep(5000);
                    System.out.println("End testSemaphoreBulkhead: " + i + "_" + Thread.currentThread().getName());
                    return i;
                } catch (Exception e) {
                    return i;
                }
            });
        }).get();
    }

    private CompletionStage<Void> testThreadpoolBulkhead() {
        return ThreadPoolBulkhead.decorateRunnable(threadPoolBulkhead, () -> {
            try {
                System.out.println("Start testThreadpoolBulkhead: " + Thread.currentThread().getName());
                Thread.sleep(5000);
                System.out.println("End testThreadpoolBulkhead: " + Thread.currentThread().getName());
            } catch (Exception e) {
                System.out.println("ERROR bulkhead");
            }
        }).get();
    }

    private CompletionStage<Integer> testRateLimiter(int i) {
        return RateLimiter.decorateCompletionStage(rateLimiter, () -> {
            return CompletableFuture.supplyAsync(() -> {
                try {
                    System.out.println("Start testRateLimiter: " + i + "_" + Thread.currentThread().getName());
                    Thread.sleep(5000);
                    System.out.println("End testRateLimiter: " + i + "_" + Thread.currentThread().getName());
                    return i;
                } catch (Exception e) {
                    return i;
                }
            });
        }).get();
    }

    private void testRetry() {
        Retry.decorateRunnable(retry, () -> {
            System.out.println("Retry time: " + counter.incrementAndGet());
            if (counter.get() != 3) {
                throw new RuntimeException("RETRY");
            } else {
                System.out.println("DONE testRetry");
            }
        }).run();
    }
}
