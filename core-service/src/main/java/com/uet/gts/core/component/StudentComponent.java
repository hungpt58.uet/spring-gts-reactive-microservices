package com.uet.gts.core.component;

import com.uet.gts.common.dto.core.StudentDTO;
import com.uet.gts.common.req.core.NewStudentReq;
import com.uet.gts.common.req.core.UpdateStudentReq;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface StudentComponent {
    Mono<StudentDTO> getById(Integer id);
    Mono<Void> create(NewStudentReq studentReq);
    Mono<Void> update(Integer studentId, UpdateStudentReq studentReq);
    Mono<Void> deleteById(Integer studentId);

    Flux<StudentDTO> getAll();
}
