package com.uet.gts.core.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
            .servers(List.of(
                    new Server().url("http://localhost:8080/core/").description("Local Envi")
                )
            )
            .info(new Info().title("Core-Service API Document")
                .contact(new Contact()
                    .email("hungpt58.uet@gmail.com")
                    .name("StormSpirit")
                    .url("http://localhost:8080/swagger-ui.html"))
                .version("1.0.0"));
    }
}
