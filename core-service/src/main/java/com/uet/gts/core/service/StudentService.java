package com.uet.gts.core.service;

import com.uet.gts.core.model.entities.StudentEntity;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Set;

public interface StudentService {
    Mono<StudentEntity> findById(Integer studentId);
    Mono<StudentEntity> createOrUpdate(StudentEntity student);
    Mono<Void> deleteById(Integer studentId);
    Mono<StudentEntity> findByCode(String code);
    Mono<Set<StudentEntity>> findAndCheckByIds(Set<Integer> studentIds);

    Flux<StudentEntity> createOrUpdate(Set<StudentEntity> students);
    Flux<StudentEntity> findAll();
}
