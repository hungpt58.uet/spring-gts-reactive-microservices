package com.uet.gts.core.component;

import reactor.core.publisher.Mono;

public interface HelloComponent {
    Mono<String> simulateCircuitBreaker();
}
