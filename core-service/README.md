# Core-service module

### General Information

- Context-base-path: `/core`
- View all api-documents: `http://localhost:8080/core/swagger-ui.html`
- View actuator: `http://localhost:8080/core/actuator`
- Using `Spring webflux` base on `Reactor-core` & non-blocking database: `r2dbc-mysql`
- Simulate relationship between teacher and student at school as main domain business
- Using mysql v8 as main database
- DB is migrated automatically by liquibase. View db config at [file properties](src/main/resources/application.yaml)

### Business logic

![](src/main/resources/document/database.jpg)

- Entities: Student & Teacher
- Relationship: One teacher manages many students. One student is only managed by one teacher.
- Domain Logic: 
  + [**_StudentEntity_**](src/main/java/com/uet/gts/core/model/entities/StudentEntity.java): Get `Age`, `Grade` and `Status` base on `DateOfBirth`
  + [**_TeacherEntity_**](src/main/java/com/uet/gts/core/model/entities/TeacherEntity.java): Add new `student` list, who isn't managed yet by anyone. Max is 15
- Permission:
  + Role_Manager: Only access `student-apis` > Add/Edit/Get their information. Except `delete-api` 
  + Role_GeneralManager: Only access `teacher-apis` > Add/Edit/Get their information
  + Role_Director: Access all apis

### Check list

1. [x] Apply Spring Webflux
2. [x] Apply Liquibase for db migration
3. [x] Apply OpenAPI document
4. [x] Apply I18N for error message
5. [x] Apply R2dbc-mysql
6. [x] Apply Caching
7. [x] Apply Eureka-client
8. [x] Apply SpringActuator
9. [x] Apply Circuit-Breaker
10. [x] Apply Spring security for authentication and authorization
11. [x] Customize response for HTTP code 404(wrong api path), 405(method not allowed)