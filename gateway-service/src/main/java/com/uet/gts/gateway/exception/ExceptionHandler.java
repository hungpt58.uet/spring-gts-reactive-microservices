package com.uet.gts.gateway.exception;

import com.uet.gts.common.dto.Response;
import com.uet.gts.common.utils.ErrorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

import java.util.Map;

import static com.uet.gts.common.constant.ErrorList.SERVER_ERROR;
import static com.uet.gts.common.constant.ErrorList.SERVER_UNAVAILABLE;

@Component
@Order(-2)
public class ExceptionHandler extends AbstractErrorWebExceptionHandler {

    private static final Integer HTTP_CODE_504 = 504;
    private static final Integer HTTP_CODE_503 = 503;
    private static final String HTTP_STATUS_KEY = "status";
    private static final String PATH_KEY = "path";

    @Autowired private ErrorFactory errorFactory;

    public ExceptionHandler(ErrorAttributes errorAttributes, ApplicationContext applicationContext, ServerCodecConfigurer serverCodecConfigurer) {
        super(errorAttributes, new WebProperties.Resources(), applicationContext);
        this.setMessageWriters(serverCodecConfigurer.getWriters());
        this.setMessageReaders(serverCodecConfigurer.getReaders());
    }

    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(ErrorAttributes errorAttributes) {
        return RouterFunctions.route(
                RequestPredicates.all(), this::renderErrorResponse);
    }

    private Mono<ServerResponse> renderErrorResponse(ServerRequest request) {
        Map<String, Object> errorPropertiesMap = getErrorAttributes(request, ErrorAttributeOptions.defaults());

        var httpCode = (int) errorPropertiesMap.get(HTTP_STATUS_KEY);
        HttpStatus httpStatus = null;
        Response<?> errorResponse = null;

        if (httpCode == HTTP_CODE_504 || httpCode == HTTP_CODE_503) {
            httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
            errorResponse = get504ErrorResponse();
        } else {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            errorResponse = get500ErrorResponse();
        }

        return ServerResponse.status(httpStatus)
                .body(Mono.just(errorResponse), Response.class);
    }

    private Response<?> get504ErrorResponse() {
        return new Response<>(errorFactory.getI18NError(SERVER_UNAVAILABLE));
    }

    private Response<?> get500ErrorResponse() {
        return new Response<>(errorFactory.getI18NError(SERVER_ERROR));
    }
}
