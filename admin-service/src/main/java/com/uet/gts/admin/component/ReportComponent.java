package com.uet.gts.admin.component;

import com.uet.gts.common.dto.admin.ReportDTO;

import java.util.List;

public interface ReportComponent {
    List<ReportDTO> getReports();
    ReportDTO getByTeacherId(Integer teacherId);
}
