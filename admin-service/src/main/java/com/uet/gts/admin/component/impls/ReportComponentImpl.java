package com.uet.gts.admin.component.impls;

import com.uet.gts.admin.component.ReportComponent;
import com.uet.gts.admin.exception.ExceptionFactory;
import com.uet.gts.admin.service.ReportService;
import com.uet.gts.common.dto.admin.ReportDTO;
import com.uet.gts.common.dto.core.TeacherDTO;
import com.uet.gts.common.utils.FutureUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.uet.gts.common.constant.ErrorList.REPORTS_ERROR;

@Service
@Slf4j
public class ReportComponentImpl implements ReportComponent {

    @Autowired private ExceptionFactory exceptionFactory;
    @Autowired private ReportService reportService;

    @Override
    public List<ReportDTO> getReports() {
        var lcf = reportService.findAllTeachers()
            .stream().map(teacher -> {
                return CompletableFuture.supplyAsync(() -> {
                    return getByTeacherId(teacher.getId());
                });
            })
            .collect(Collectors.toList());

        var cfl = FutureUtil.sequence(lcf)
            .exceptionally(ex -> {
                log.error(String.format("Error %s: %s", REPORTS_ERROR, ex.getMessage()));
                throw exceptionFactory.businessException(REPORTS_ERROR);
            });
        return cfl.join();
    }

    @Override
    public ReportDTO getByTeacherId(Integer teacherId) {
        var detailTeacherOpt = reportService.findDetailTeacherById(teacherId);
        if (detailTeacherOpt.isEmpty()) {
            throw exceptionFactory.recordNotFoundException(TeacherDTO.Fields.id);
        }

        var teacherDTO = detailTeacherOpt.get();
        var studentDTOs = teacherDTO.getStudents();
        teacherDTO.setStudents(null);

        return ReportDTO.builder()
            .teacher(teacherDTO)
            .students(studentDTOs)
            .build();
    }
}
