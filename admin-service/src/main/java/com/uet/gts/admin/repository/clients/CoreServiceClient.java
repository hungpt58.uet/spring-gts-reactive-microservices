package com.uet.gts.admin.repository.clients;

import com.uet.gts.common.constant.apis.CorePath;
import com.uet.gts.common.dto.Response;
import com.uet.gts.common.dto.core.TeacherDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "${service.core.path}")
public interface CoreServiceClient {
    @GetMapping(value = CorePath.Internal.TEACHERS)
    Response<List<TeacherDTO>> getAllTeachers();

    @GetMapping(value = CorePath.Internal.TEACHER)
    Response<TeacherDTO> getTeacherById(@PathVariable("id") Integer teacherId);
}
