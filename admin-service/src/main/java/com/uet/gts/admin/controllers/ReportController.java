package com.uet.gts.admin.controllers;

import com.uet.gts.admin.component.ReportComponent;
import com.uet.gts.common.constant.apis.AdminPath;
import com.uet.gts.common.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = AdminPath.REPORT)
public class ReportController {

    @Autowired private ReportComponent reportComponent;

    @GetMapping("")
    public Response getReports() {
        return new Response<>(
            reportComponent.getReports()
        );
    }
}
