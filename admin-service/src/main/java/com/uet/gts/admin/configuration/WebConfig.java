package com.uet.gts.admin.configuration;

import io.github.resilience4j.common.retry.configuration.RetryConfigCustomizer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@Slf4j
public class WebConfig {
    public static final String EXTERNAL_SERVICE = "external-service";

    //=====================[ Resilience4j Customizer Config : Need file config ]=======================================
    /* Resilience4j Retry config */
    @Bean
    public RetryConfigCustomizer getRetryConfig() {
        return RetryConfigCustomizer.of(EXTERNAL_SERVICE, builder -> {
           builder.waitDuration(Duration.ofMillis(500))
                   .maxAttempts(1)
                   .failAfterMaxAttempts(true)
                   .build();
        });
    }
}
