package com.uet.gts.admin.exception;

import com.uet.gts.common.exceptions.BusinessException;
import com.uet.gts.common.exceptions.RecordNotFoundException;
import com.uet.gts.common.utils.ErrorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.uet.gts.common.constant.ErrorList.RECORD_NOT_FOUND;

@Component
public class ExceptionFactory {
    @Autowired
    private ErrorFactory errorFactory;

    public RecordNotFoundException recordNotFoundException(String fieldName) {
        return new RecordNotFoundException(
            errorFactory.getI18NError(RECORD_NOT_FOUND, fieldName)
        );
    }

    public BusinessException businessException(String i18nKey, String fieldName) {
        return new BusinessException(
            errorFactory.getI18NError(i18nKey, fieldName)
        );
    }

    public BusinessException businessException(String i18nKey) {
        return new BusinessException(
            errorFactory.getI18NError(i18nKey)
        );
    }
}
