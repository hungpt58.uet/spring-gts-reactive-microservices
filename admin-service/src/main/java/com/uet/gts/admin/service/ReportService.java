package com.uet.gts.admin.service;

import com.uet.gts.common.dto.core.TeacherDTO;

import java.util.List;
import java.util.Optional;

public interface ReportService {
    List<TeacherDTO> findAllTeachers();
    Optional<TeacherDTO> findDetailTeacherById(Integer teacherId);
}
