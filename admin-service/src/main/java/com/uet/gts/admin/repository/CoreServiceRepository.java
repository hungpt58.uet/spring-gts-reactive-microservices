package com.uet.gts.admin.repository;

import com.uet.gts.common.dto.core.TeacherDTO;

import java.util.List;
import java.util.Optional;

public interface CoreServiceRepository {
    List<TeacherDTO> findAllTeachers();
    Optional<TeacherDTO> findDetailTeacherById(Integer teacherId);
}
