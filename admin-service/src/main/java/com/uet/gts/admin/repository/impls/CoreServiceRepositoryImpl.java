package com.uet.gts.admin.repository.impls;

import com.uet.gts.admin.configuration.WebConfig;
import com.uet.gts.admin.repository.CoreServiceRepository;
import com.uet.gts.admin.repository.clients.CoreServiceClient;
import com.uet.gts.common.dto.core.TeacherDTO;
import feign.FeignException;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Repository
@CircuitBreaker(name = WebConfig.EXTERNAL_SERVICE)
@RateLimiter(name = WebConfig.EXTERNAL_SERVICE)
@Bulkhead(name = WebConfig.EXTERNAL_SERVICE)
@Retry(name = WebConfig.EXTERNAL_SERVICE)
public class CoreServiceRepositoryImpl implements CoreServiceRepository {

    @Autowired private CoreServiceClient coreServiceClient;

    AtomicInteger x = new AtomicInteger(1);

    @Override
    public List<TeacherDTO> findAllTeachers() {
        System.out.println("Counter: " + x.incrementAndGet());
        return coreServiceClient.getAllTeachers().getData();
    }

    @Override
    public Optional<TeacherDTO> findDetailTeacherById(Integer teacherId) {
        try {
            return Optional.of(
                coreServiceClient.getTeacherById(teacherId).getData()
            );
        } catch (FeignException e) {
            if (e.status() == HttpStatus.NOT_FOUND.value())
                return Optional.empty();
            else
                throw e;
        }
    }
}
