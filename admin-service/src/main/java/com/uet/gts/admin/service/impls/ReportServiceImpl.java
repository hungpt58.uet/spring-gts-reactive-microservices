package com.uet.gts.admin.service.impls;

import com.uet.gts.admin.repository.CoreServiceRepository;
import com.uet.gts.admin.service.ReportService;
import com.uet.gts.common.dto.core.TeacherDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired private CoreServiceRepository coreRepository;

    @Override
    public List<TeacherDTO> findAllTeachers() {
        return coreRepository.findAllTeachers();
    }

    @Override
    public Optional<TeacherDTO> findDetailTeacherById(Integer teacherId) {
        return coreRepository.findDetailTeacherById(teacherId);
    }
}
