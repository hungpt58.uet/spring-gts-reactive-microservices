# Common-lib module

### Structure

```
com.uet.gts.common
    │
    └───> annotation: Custom annotations and its contrainst
    │   
    └───> constant: Regex & ErrorList & Enums
    │    
    └───> dto: DTO definitions, includes > common & admin & auth & cỏe
    │   
    └───> exceptions: Common exceptions
    │   
    └───> req: Request object definitions, includes > auth & core
    │   
    └───> utils: Utility classes
```
