package com.uet.gts.common.annotation.constraints;

import com.uet.gts.common.annotation.ValidIdList;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.List;

/**
 * Check all id in list must be smaller than 0 and not duplicated
 */
public class IdListConstraint implements ConstraintValidator<ValidIdList, List<Integer>> {
    @Override
    public boolean isValid(List<Integer> values, ConstraintValidatorContext context) {
        if (values == null) return true; // Remember: This logic is checked by another validator

        var smallCounter = (int) values.stream().filter(v -> v <= 0).count();
        var setValues = new HashSet<>(values);

        return setValues.size() == values.size() || smallCounter == 0;
    }
}
