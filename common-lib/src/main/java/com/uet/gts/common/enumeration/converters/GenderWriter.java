package com.uet.gts.common.enumeration.converters;

import com.uet.gts.common.enumeration.Gender;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

@WritingConverter
public class GenderWriter implements Converter<Gender, String> {
    @Override
    public String convert(Gender source) {
        return source.getValue();
    }
}
