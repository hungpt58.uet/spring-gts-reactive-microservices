package com.uet.gts.common.exceptions;

import com.uet.gts.common.dto.ErrorDTO;

public class RecordNotFoundException extends BusinessException {
    public RecordNotFoundException(ErrorDTO errorDTO) {
        super(errorDTO);
    }
}
