package com.uet.gts.common.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Gender {
    MALE("male"), FEMALE("female");

    @Getter private String value;
}
