package com.uet.gts.common.req.core;

import com.uet.gts.common.annotation.ValidIdList;
import com.uet.gts.common.req.Request;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@FieldNameConstants
public class StudentIdListReq extends Request {
    @NotNull(message = "{validation.field.required}")
    @NotEmpty(message = "{validation.list.not-empty}")
    @ValidIdList(message = "{validation.core.student.list-id.invalid}")
    private List<Integer> studentIds;
}
