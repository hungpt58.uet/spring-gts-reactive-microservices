package com.uet.gts.common.enumeration.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

import java.util.Optional;

@WritingConverter
public class OptionalFieldWriter implements Converter<Optional<Object>, Object> {
    @Override
    public Object convert(Optional<Object> source) {
        return source.orElse(null);
    }
}
