package com.uet.gts.common.req.auth;

import com.uet.gts.common.req.Request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@FieldNameConstants
public class RegisterReq extends Request {

    @NotNull(message = "{validation.auth.field.required}")
    @Size(max = 100, message = "{validate.auth.field.length.max}")
    private String username;

    @NotNull(message = "{validation.auth.field.required}")
    private String password;

    @NotNull(message = "{validation.auth.field.required}")
    @Email(message = "{validation.auth.field.email.format}")
    private String email;
}
