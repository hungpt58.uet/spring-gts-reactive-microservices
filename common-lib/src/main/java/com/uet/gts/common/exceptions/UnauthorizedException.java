package com.uet.gts.common.exceptions;

import com.uet.gts.common.dto.ErrorDTO;

public class UnauthorizedException extends BaseException {

    private final static String CODE    = "ERR401";
    private final static String MESSAGE = "Authentication is failed";

    public UnauthorizedException() {
        super(MESSAGE);
    }

    public ErrorDTO getError() {
        return ErrorDTO.builder()
                .code(CODE)
                .message(this.getMessage())
                .build();
    }
}
