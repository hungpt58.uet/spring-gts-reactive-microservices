package com.uet.gts.common.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum StudentStatus {
    NOT_JOIN(0), JOINED(1), GRADUATED(2);

    @Getter private Integer code;

    @Override
    public String toString() {
        switch (this) {
            case NOT_JOIN:  return "notJoinYet";
            case JOINED:    return "joined";
            case GRADUATED: return "graduated";

            default: return null;
        }
    }
}
