package com.uet.gts.common.req.core;

import com.uet.gts.common.annotation.DateFormat;
import com.uet.gts.common.req.Request;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@FieldNameConstants
public class UpdateStudentReq extends Request {
    @NotNull(message = "{validation.field.required}")
    @Size(min = 1, max = 100, message = "{validate.field.length}")
    private String name;

    @NotNull(message = "{validation.field.required}")
    @DateFormat(message = "{validation.date.format}")
    @Past(message = "{validation.date.past}")
    private LocalDate dateOfBirth;
}
