package com.uet.gts.common.exceptions;

import com.uet.gts.common.dto.ErrorDTO;

public class BusinessException extends BaseException {

    private final ErrorDTO error;

    public BusinessException(ErrorDTO errorDTO) {
        super(errorDTO.getMessage());
        this.error = errorDTO;
    }

    public ErrorDTO getError() {
        return this.error;
    }
}
