package com.uet.gts.common.annotation.constraints;

import com.uet.gts.common.annotation.DateFormat;
import com.uet.gts.common.utils.TimeUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.util.Date;

public class DateFormatConstraint implements ConstraintValidator<DateFormat, Object> {

    private String pattern;

    @Override
    public void initialize(DateFormat constraintAnnotation) {
        this.pattern = constraintAnnotation.pattern();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value instanceof LocalDate) return true;
        if (value instanceof Date)      return true;

        try {
            return TimeUtil.convertToDate((String) value, pattern) != null;
        } catch (NullPointerException e) {
            return true;
        } catch (Throwable e) {
            return false;
        }
    }
}
