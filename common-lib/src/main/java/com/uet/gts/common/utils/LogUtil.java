package com.uet.gts.common.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

public class LogUtil {
    public static String convertStackTraceToString(Throwable e) {
        if (e == null) return "";

        var sw = new StringWriter();
        var pw = new PrintWriter(sw);
        e.printStackTrace(pw);

        return sw.toString();
    }
}
