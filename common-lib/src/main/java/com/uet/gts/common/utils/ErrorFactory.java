package com.uet.gts.common.utils;

import com.uet.gts.common.dto.ErrorDTO;
import org.springframework.context.MessageSource;

import java.util.Locale;

public class ErrorFactory {

    private final MessageSource messageSource;
    private static final String SEPARATE_SYMBOL = "~";

    public ErrorFactory(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public ErrorDTO getI18NError(String i18nKey) {
        var message = messageSource.getMessage(i18nKey, null, Locale.getDefault());
        var separatedValues = message.split(SEPARATE_SYMBOL);

        return ErrorDTO.builder()
                .code(separatedValues[0])
                .message(separatedValues[1])
                .build();
    }

    public ErrorDTO getI18NError(String i18nKey, String fieldName) {
        var errorDTO = getI18NError(i18nKey);
        errorDTO.setFieldName(fieldName);

        return errorDTO;
    }

    public ErrorDTO getError(String message, String fieldName) {
        var separatedValues = message.split(SEPARATE_SYMBOL);
        return ErrorDTO.builder()
                .fieldName(fieldName)
                .code(separatedValues[0])
                .message(separatedValues[1])
                .build();
    }
}
