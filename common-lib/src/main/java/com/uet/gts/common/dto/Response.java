package com.uet.gts.common.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.uet.gts.common.utils.TimeUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Set;

@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response <T> {
    @Getter private T data;
    @Getter private Set<ErrorDTO> errors;
    @Getter private String time = TimeUtil.nowDateTimeStr();

    public Response(ErrorDTO errorDTO) {
        this.errors = Set.of(errorDTO);
    }

    public Response(Set<ErrorDTO> errors) {
        this.errors = errors;
    }

    public Response(T data) {
        this.data = data;
    }
}
