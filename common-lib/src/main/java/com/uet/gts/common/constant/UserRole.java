package com.uet.gts.common.constant;

public class UserRole {
    public static final String DIRECTOR = "director";
    public static final String GENERAL_MANAGER = "general_manager";
    public static final String MANAGER = "manager";
    public static final String OPERATOR = "operator";
}
