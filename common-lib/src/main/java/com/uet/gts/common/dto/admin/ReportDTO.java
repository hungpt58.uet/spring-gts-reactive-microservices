package com.uet.gts.common.dto.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.uet.gts.common.dto.core.StudentDTO;
import com.uet.gts.common.dto.core.TeacherDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportDTO {
    private TeacherDTO teacher;
    private Set<StudentDTO> students;
}
