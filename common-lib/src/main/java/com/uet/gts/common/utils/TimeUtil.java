package com.uet.gts.common.utils;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimeUtil {
    public final static String DATE_FORMAT = "yyyy-MM-dd";
    public final static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    //==========[ Convert from String to LocalDateTime ]============================
    public static LocalDate convertToDate(String value) throws ParseException {
        return LocalDate.parse(value, DateTimeFormatter.ofPattern(DATE_FORMAT));
    }

    public static LocalDate convertToDate(String value, String format) {
        return LocalDate.parse(value, DateTimeFormatter.ofPattern(format));
    }

    //==========[ Convert from LocalDateTime to String ]============================
    public static String nowDateTimeStr() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(TimeUtil.DATE_TIME_FORMAT));
    }

    public static String convertToDateStr(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern(DATE_FORMAT));
    }

    public static String convertToDateStr(LocalDate date, String format) {
        return date.format(DateTimeFormatter.ofPattern(format));
    }
}
