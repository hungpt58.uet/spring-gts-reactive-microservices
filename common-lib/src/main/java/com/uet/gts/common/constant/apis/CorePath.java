package com.uet.gts.common.constant.apis;

public class CorePath {
    public static final String CONTEXT = "/core";

    public static final String HELLO = "/api/v1/hello";
    public static final String TEST  = "/api/v1/circuit-breaker";

    public static final String SWAGGER = "/swagger-ui.html";
    public static final String V3_DOC = "/v3/api-docs/**";
    public static final String WEBJARS = "/webjars/**";

    public static final String STUDENTS = "/api/v1/students";
    public static final String STUDENT = "/api/v1/students/{id}";

    public static final String TEACHERS = "/api/v1/teachers";
    public static final String TEACHER = "/api/v1/teachers/{id}";
    public static final String TEACHER_STUDENT = "/api/v1/teachers/{id}/students";

    public static class Internal {
        public static final String TEACHERS = "/api/internal/teachers";
        public static final String TEACHER = "/api/internal/teachers/{id}";
    }
}
