package com.uet.gts.common.utils;

import java.util.Set;
import java.util.stream.Collectors;

public class CommonFunc {
    /**
     * Get A's elements are not in B's elements
     */
    public static <T> Set<T> getDiffFromA2B(Set<T> A, Set<T> B) {
        return A.stream().filter(t -> !B.contains(t)).collect(Collectors.toSet());
    }
}
