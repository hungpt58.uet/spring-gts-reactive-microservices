package com.uet.gts.common.constant;

public class Regex {
    public static final String GENDER = "^male|female$";
    public static final String CODE = "^[a-zA-Z0-9]{6}$";
    public static final String DATE = "^\\d{4}\\/(0[1-9]|1[012])\\/(0[1-9]|[12][0-9]|3[01])$";
}
