package com.uet.gts.common.annotation;

import com.uet.gts.common.annotation.constraints.DateFormatConstraint;
import com.uet.gts.common.utils.TimeUtil;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DateFormatConstraint.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DateFormat {
    String pattern() default TimeUtil.DATE_FORMAT;
    String message() default "";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
