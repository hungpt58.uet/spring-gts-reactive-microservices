package com.uet.gts.common.exceptions;

import com.uet.gts.common.dto.ErrorDTO;

public class PermissionDeniedException extends BaseException {

    private final static String CODE    = "ERR403";
    private final static String MESSAGE = "No permission to access this resource";

    public PermissionDeniedException() {
        super(MESSAGE);
    }

    public ErrorDTO getError() {
        return ErrorDTO.builder()
                .code(CODE)
                .message(this.getMessage())
                .build();
    }
}
