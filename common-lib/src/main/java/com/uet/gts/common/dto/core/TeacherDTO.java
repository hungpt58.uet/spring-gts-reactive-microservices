package com.uet.gts.common.dto.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@FieldNameConstants
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TeacherDTO {
    private Integer id;
    private String code;
    private String name;

    private Set<StudentDTO> students;
}
