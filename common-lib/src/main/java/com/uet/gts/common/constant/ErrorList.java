package com.uet.gts.common.constant;

public class ErrorList {
    public static final String SERVER_ERROR        = "system.error";
    public static final String SERVER_UNAVAILABLE  = "system.unavailable";
    public static final String METHOD_NOT_SUPPORT  = "system.method.not-support";
    public static final String PATH_NOT_FOUND      = "system.api.path.not-found";
    public static final String SERVER_FORBIDDEN    = "system.forbidden";
    public static final String SERVER_UNAUTHORIZED = "system.unauthorized";

    public static final String CODE_EXISTED         = "validation.core.code.existed";
    public static final String REQUEST_WRONG_FORMAT = "validation.request.body.wrong";
    public static final String RECORD_NOT_FOUND     = "validation.record.not-found";
    public static final String ADD_STUDENT_FAILED   = "validation.core.teacher.students.failed";
    public static final String STUDENT_IDS_INVALID  = "validation.core.student.list-id.invalid";

    public static final String REPORTS_ERROR = "validation.admin.report.error";
}
