package com.uet.gts.common.enumeration.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.util.Optional;

@ReadingConverter
public class OptionalFieldReader implements Converter<String, Optional<String>> {

    @Override
    public Optional<String> convert(String source) {
        if (source == null) return Optional.empty();
        else                return Optional.of(source);
    }
}
