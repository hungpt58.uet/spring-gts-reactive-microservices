package com.uet.gts.common.annotation.constraints;

import com.uet.gts.common.annotation.Gender;
import com.uet.gts.common.constant.Regex;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class GenderConstraint implements ConstraintValidator<Gender, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) return true;

        var pattern = Pattern.compile(Regex.GENDER);
        return pattern.matcher(value).find();
    }
}
